# Recipes For Everyone - iOS Development Project

## Specifications
* Recipes can be created locally
* Recipes from API can be stored offline
* API-access to search for public recipes
* Timer function - send push notification when a timer reaches its limit
* Usage of the camera to take picture of the food
* Some animations when switching views
* MVC-structure of the code
* Pragma marks
* Well structured code